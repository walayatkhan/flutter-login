import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyLogin(),
    );
  }
}

class MyLogin extends StatelessWidget {
  const MyLogin({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(40),
                  bottomRight: Radius.circular(40)),
              child: Image.asset(
                'assets/images/bg-login.jpeg',
                colorBlendMode: BlendMode.hardLight,
                color: Color(0xFFB04EF8).withOpacity(0.5),
              ),
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Container(
                margin: EdgeInsets.only(top: 50, left: 130),
                child: Text(
                  'App Name',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w900),
                ),
              )
            ]),
            Stack(
              children: [
                Positioned(
                  top: 200,
                  height: MediaQuery.of(context).size.height - 20,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width - 20,
                        // height: MediaQuery.of(context).size.height,
                        // width: MediaQuery.of(context).size.width - 40,
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // border: Border.all(color: Color(0xFFB04EF8)),
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFB04EF8).withOpacity(0.1),
                                  spreadRadius: 5)
                            ]),

                        child: Column(
                          children: [
                            LoginTitle(),
                            UsernameTextField(),
                            Divider(),
                            PasswordTextField(),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height - 700),
                            LoginButton(),
                            SizedBox(height: 50),
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 0),
                                  child: Container(
                                    child: Text('I don\'t have account.'),
                                  ),
                                ),
                                SizedBox(height: 50),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class LoginButton extends StatelessWidget {
  const LoginButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: ElevatedButton(
        onPressed: () {},
        child: Text(
          'Login',
          style: TextStyle(fontWeight: FontWeight.w900, fontSize: 20.0),
        ),
        style: ElevatedButton.styleFrom(
          primary: Color(0xFFB04EF8),
          padding: EdgeInsets.symmetric(horizontal: 110, vertical: 17),
        ),
      ),
    );
  }
}

class PasswordTextField extends StatelessWidget {
  const PasswordTextField({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var inputDecoration = InputDecoration(
        labelText: 'Password',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(color: Colors.grey),
        ),
        prefixIcon: Icon(
          Icons.lock,
          color: Color(0xFFB04EF8),
        ),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFB04EF8)),
            borderRadius: BorderRadius.circular(10)),
        hintText: 'Enter Password');
    return Container(
      width: MediaQuery.of(context).size.width - 60,
      child: TextFormField(
        decoration: inputDecoration,
      ),
    );
  }
}

class UsernameTextField extends StatelessWidget {
  const UsernameTextField({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var inputDecoration = InputDecoration(
        labelText: 'Username',
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(color: Colors.grey)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFB04EF8)),
            borderRadius: BorderRadius.circular(10)),
        prefixIcon: Icon(
          Icons.person,
          color: Color(0xFFB04EF8),
        ),
        hintText: 'Enter Username');
    return Container(
      width: MediaQuery.of(context).size.width - 60,
      child: TextFormField(
        decoration: inputDecoration,
      ),
    );
  }
}

class LoginTitle extends StatelessWidget {
  const LoginTitle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 30, bottom: 30),
          child: Text(
            "Login",
            style: TextStyle(
              color: Color(0xFFB04EF8),
              fontSize: 24,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ],
    );
  }
}
